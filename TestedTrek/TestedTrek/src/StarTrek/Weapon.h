#ifndef WEAPON_H
#define WEAPON_H

#include <string>

class Weapon
{
public:
    enum WeaponType
    {
        Unknown,
        Phaser,
        Photon,
        numWeaponTypes
    };

    Weapon(WeaponType type);

    void fire();

private:
    const WeaponType m_type;
    std::string m_name;
};

#endif // WEAPON_H
