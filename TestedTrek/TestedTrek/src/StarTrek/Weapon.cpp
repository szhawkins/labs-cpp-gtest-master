#include "Weapon.h"

Weapon::Weapon(Weapon::WeaponType type) :
    m_type(type)
{
    switch (m_type)
    {
    case Phaser:
        m_name = "phaser";
        break;

    case Photon:
        m_name = "photon";
        break;

    default:
        m_name = "unknown";
        break;
    }
}

void Weapon::fire()
{
}
